package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PrimeNumber {

    public static void main(String[] args) {
        // Array

        int primeNumber[] = new int[5];

        primeNumber[0] = 2;
        primeNumber[1] = 3;
        primeNumber[2] = 5;
        primeNumber[3] = 7;
        primeNumber[4] = 11;



        System.out.println("The first prime number is: " + primeNumber[0]);

        //ArrayList

        ArrayList<String> friends = new ArrayList<String>();

        // Add Elements
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My friends are: " + friends);

        // HashMap

        HashMap<String, Integer> toiletries = new HashMap<String, Integer>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };
        System.out.println("Our current inventory consists of: " + toiletries);
    }
}
