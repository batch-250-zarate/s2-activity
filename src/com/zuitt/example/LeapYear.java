package com.zuitt.example;

import java.util.Scanner;

public class LeapYear {
    public  static void main(String[] args) {
        Scanner leapYear = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year: ");
        int leapYearValue = leapYear.nextInt();

        if (leapYearValue % 4 == 0) {

            if (leapYearValue % 100 == 0) {

                if (leapYearValue % 400 == 0) {
                    System.out.println(leapYearValue + " is a leap year");
                }
                else {
                    System.out.println(leapYearValue + " is NOT a leap year");
                }
            }
            else {
                System.out.println(leapYearValue + " is a leap year");
            }
        }
        else {
            System.out.println(leapYearValue + " is NOT a leap year");
        }
    }
}
